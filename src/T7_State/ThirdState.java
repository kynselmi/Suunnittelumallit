/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T7_State;

/**
 *
 * @author AK
 */
public class ThirdState implements DevelopmentState {

    private final static ThirdState INSTANCE = new ThirdState();

    public static ThirdState getInstance() {
        return INSTANCE;
    }

    @Override
    public void run() {
        System.out.println("Run like a wind");
    }

    @Override
    public void attack() {
        System.out.println("Attack fiercely");
    }

    @Override
    public void takeCover() {
        System.out.println("Make a bunker");
    }

    @Override
    public void heal() {
        System.out.println("Do a surgery on yourself");
    }

}
