/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T7_State;

/**
 *
 * @author AK
 */
public class SecondState implements DevelopmentState {

    private final static SecondState INSTANCE = new SecondState();

    public static SecondState getInstance() {
        return INSTANCE;
    }

    @Override
    public void run() {
        System.out.println("Run faster than first timer");
    }

    @Override
    public void attack() {
        System.out.println("Attack a lot more");
    }

    @Override
    public void takeCover() {
        System.out.println("Hide behind a tree");
    }

    @Override
    public void heal() {
        System.out.println("Bandage a wound");
    }

}
