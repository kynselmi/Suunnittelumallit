/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9_Strategy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) {
        ListConverter lc = new ListConverterSimple();
        List<String> list = createList();
        
        System.out.println(lc.listToString(list));
        
        lc = new ListConverterPairs();
        System.out.println(lc.listToString(list));
        
        lc = new ListConverterThirds();
        System.out.println(lc.listToString(list));

    }

    public static List<String> createList() {
        List<String> list = new ArrayList<>();
        list.add("Minä");
        list.add("teen");
        list.add("listoilla");
        list.add("hauskoja");
        list.add("asioita");
        list.add("koska");
        list.add("minun");
        list.add("käskettiin");
        list.add("tehdä");
        list.add("niin");
        list.add("opettajan");
        list.add("ja");
        list.add("kouluni");
        list.add("Metropolian");
        list.add("toimesta.");
        
        return list;
    }

}
