/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9_Strategy;

import java.util.List;

/**
 *
 * @author AK
 */
public class ListConverterThirds implements ListConverter {

    @Override
    public String listToString(List list) {
        Object[] listArray = listToArray(list);

        String toReturn = "";

        for (int i = 0; i < listArray.length; i++) {
            toReturn += listArray[i]+" ";
            if ((i+1)%3 == 0) {
                toReturn += "\n";
            }
        }
        return toReturn;
    }

    public Object[] listToArray(List list) {
        String[] listArray = new String[list.size()];

        for (int i = 0; i < list.size(); i++) {
            listArray[i] = list.get(i).toString();
        }
        return listArray;
    }

}
