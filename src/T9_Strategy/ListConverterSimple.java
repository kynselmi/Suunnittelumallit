/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9_Strategy;

import java.util.List;

/**
 *
 * @author AK
 */
public class ListConverterSimple implements ListConverter {

    @Override
    public String listToString(List list) {
        String toReturn = "";
        for (int i = 0; i<list.size(); i++) {
            toReturn += list.get(i)+"\n";
        }
        return toReturn;
    }
    
    
    
}
