/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9_Strategy;

import java.util.Iterator;
import java.util.List;

/**
 *
 * @author AK
 */
public class PairIterator implements Iterator {
    
    private List list;
    private int index;
    
    public PairIterator(List list) {
        this.list = list;
    }

    @Override
    public boolean hasNext() {
        if(index>=list.size()) {
            return false;
        }
        return true;
    }

    @Override
    public Object next() {
        String toReturn = list.get(index).toString();
        index++;
        if (hasNext()) {
            toReturn += " "+list.get(index).toString()+"\n";
        }
        index++;
        if (!hasNext()) {
            toReturn += "\n";
        }
        return toReturn;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
