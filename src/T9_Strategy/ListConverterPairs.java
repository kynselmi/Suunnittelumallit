/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T9_Strategy;

import java.util.List;

/**
 *
 * @author AK
 */
public class ListConverterPairs implements ListConverter {
    
    private PairIterator iterator;
    

    @Override
    public String listToString(List list) {
        iterator = new PairIterator(list);
        
        String toReturn = "";
        while(iterator.hasNext()) {
            toReturn += iterator.next();
        }
        return toReturn;
        
    }
    
}
