/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15_Adapter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aleksi
 */
public class TextHolder {
    
    private List<String> listOfChars;
    
    public TextHolder(List<String> list) {
        listOfChars = list;
    }

    public List<String> getListOfChars() {
        return listOfChars;
    }
    
    
    
}
