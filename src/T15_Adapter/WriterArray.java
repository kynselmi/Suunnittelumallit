/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15_Adapter;

import java.util.List;

/**
 *
 * @author AK
 */
public class WriterArray implements Writer {

    private String[] store;
    private int indexLast;

    public WriterArray(String[] array) {
        store = array;
        indexLast = 0;

    }

    @Override
    public void append(String text) {
        if (store[0] == null) {
            store[0] = text;
        } else {
            String toBeAppended = store[indexLast];
            if (toBeAppended != null) {
                toBeAppended += text;
            } else {
                toBeAppended = text;
            }
            store[indexLast] = toBeAppended;
        }
    }

    @Override
    public void writeLine(String text) {
        indexLast++;
        store[indexLast] = text;

    }

    @Override
    public void writeFromList(List<String> list) {
        for (String listItem : list) {
            indexLast++;
            store[indexLast] = listItem;
        }
    }

}
