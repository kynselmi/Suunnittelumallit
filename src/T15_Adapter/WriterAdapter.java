/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15_Adapter;

import java.util.List;

/**
 *
 * @author Aleksi
 */
public interface WriterAdapter {

    void append(TextHolder teksti);
    void writeLine(TextHolder teksti);
    void writeFromList(TextHolder[] teksti);
}
