/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15_Adapter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Writer lWriter = new WriterList(list);
        //writeEx(lWriter);

        String[] array = new String[100];
        Writer aWriter = new WriterArray(array);
        //writeEx(aWriter);

        WriterAdapter adapter = new WriterAdapterConcrete(lWriter);

        List<String> t1List = new ArrayList<String>();
        t1List.add("J");
        t1List.add("e");
        t1List.add("e");
        TextHolder t1 = new TextHolder(t1List);

        List<String> t2List = new ArrayList<String>();
        t2List.add("M");
        t2List.add("o");
        t2List.add("i");
        t2List.add(" ");
        TextHolder t2 = new TextHolder(t2List);

        List<String> t3List = new ArrayList<String>();
        t3List.add("T");
        t3List.add("e");
        t3List.add("h");
        t3List.add("t");
        t3List.add("ä");
        t3List.add("v");
        t3List.add("ä");
        t3List.add("ä");
        TextHolder t3 = new TextHolder(t3List);

        adapter.writeLine(t2);
        adapter.append(t1);
        adapter.writeLine(t3);

        for (String text : list) {
            System.out.println(text);
        }

        TextHolder[] textHolderArray = new TextHolder[3];
        textHolderArray[0] = t1;
        textHolderArray[1] = t2;
        textHolderArray[2] = t3;

        for (String text : list) {
            System.out.println(text);
        }
    }

    public static void writeEx(Writer writer) {
        writer.append("Jee");
        writer.append(" olen teksti.");
        writer.writeLine("Olen uusi rivi");
    }

    public static void testWriter() {
        List<String> list = new ArrayList<>();
        Writer lWriter = new WriterList(list);
        writeEx(lWriter);

        String[] array = new String[100];
        Writer aWriter = new WriterArray(array);
        writeEx(aWriter);

        System.out.println(list);

        for (String s : array) {
            if (s != null) {
                System.out.println(s);
            }
        }
    }

}
