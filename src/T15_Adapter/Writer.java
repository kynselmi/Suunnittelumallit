/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15_Adapter;

import java.util.List;

/**
 *
 * @author AK
 */
public interface Writer {
    
    void append (String text);
    void writeLine (String text);
    void writeFromList (List<String> list);
    
}
