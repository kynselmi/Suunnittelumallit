/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15_Adapter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AK
 */
public class WriterList implements Writer {

    private List<String> store;
    
   
    public WriterList(List<String> list) {
        store = list;
    }

    @Override
    public void append(String text) {
        if (store.size() <= 0) {
            store.add(text);
        } else {
            String toBeAppended = store.get(store.size() - 1);
            toBeAppended += text;
            store.set(store.size() - 1, toBeAppended);
        }
    }

    @Override
    public void writeLine(String text) {
        store.add(text);
    }

    @Override
    public void writeFromList(List<String> list) {
        for (String listItem : list) {
            writeLine(listItem);
        }
    }

}
