/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T15_Adapter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aleksi
 */
public class WriterAdapterConcrete implements WriterAdapter {
    
    private Writer writer;
    
    public WriterAdapterConcrete(Writer writer) {
        this.writer = writer;
    }

    @Override
    public void append(TextHolder text) {
        writer.append(buildString(text));
    }

    @Override
    public void writeLine(TextHolder text) {
        writer.writeLine(buildString(text));
    }

    @Override
    public void writeFromList(TextHolder[] list) {
        List<String> listOfText = new ArrayList<String>();
        for (TextHolder textHolder : list) {
            listOfText.add(buildString(textHolder));
        }
        writer.writeFromList(listOfText);
    }

    private String buildString(TextHolder textHolder) {
        String string = "";
        for (String character : textHolder.getListOfChars()) {
            string += character;
        }
        return string;
    }
}
