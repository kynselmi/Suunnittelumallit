/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T20_Iterator;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AK
 */
public class ThreadController {
    
    private List<Thread> list = new ArrayList<>();
    private int threadTurn;
    
    public void addThread(Thread t) {
        list.add(t);
    }
    
    public int getTurn() {
        return threadTurn+1;
    }
    
    public void nextTurn() {
        threadTurn++;
        if (threadTurn >= list.size()) {
            threadTurn = 0;
        } 
    }
    
}
