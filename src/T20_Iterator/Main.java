/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T20_Iterator;

import static java.lang.Thread.sleep;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add(i);
        }
        Iterator itr1 = list.iterator();
        Iterator itr2 = list.iterator();
        ThreadController tc = new ThreadController();
        IteratorUserThread t1 = new IteratorUserThread(itr1, 1, tc);
        IteratorUserThread t2 = new IteratorUserThread(itr1, 2, tc);
        tc.addThread(t1);
        tc.addThread(t2);

        t1.start();
        t1.join();
        t2.start();

        //Lisätään kesken iteroinnin
        //list.add(55);
        
    }
}
