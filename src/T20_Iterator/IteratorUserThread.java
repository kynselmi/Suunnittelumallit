/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T20_Iterator;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AK
 */
public class IteratorUserThread extends Thread {

    Iterator itr;
    int numberOfThread;
    ThreadController tc;

    public IteratorUserThread(Iterator itr, int numberOfThread, ThreadController tc) {
        this.itr = itr;
        this.numberOfThread = numberOfThread;
        this.tc = tc;
    }

    @Override
    public void run() {
        int n = 0;
        while (itr.hasNext()) {
            synchronized (itr) {
                if (!itr.hasNext()) break;
                iterate();
                n++;
                if (n == 25) break;
            }
        }
    }

    private void iterate() {
        //if (tc.getTurn() == numberOfThread) {
            System.out.println("Thread " + numberOfThread + ": " + itr.next());
            tc.nextTurn();
            System.out.println("Iteroitu");
        //}

    }
}
