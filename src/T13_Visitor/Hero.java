/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13_Visitor;

import T7_State.*;

/**
 *
 * @author AK
 */
public class Hero {
    
    protected DevelopmentState state;
    protected int xpPoints;
    
    public Hero() {
        this.state = FirstState.getInstance();
    }
    
    public void changeState(DevelopmentState s) {
        this.state = s;
    }
    
    public void run() {
        state.run();
    }
    
    public void attack() {
        state.attack();
    }
    
    public void takeCover() {
        state.takeCover();
    }
    
    public void heal() {
        state.heal();
    }
    
    void accept (BonusVisitor visitor) {
        visitor.visit(this);
    }

    public int getXpPoints() {
        return xpPoints;
    }
    
    
}
