/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13_Visitor;

import T7_State.*;

/**
 *
 * @author AK
 */
public interface DevelopmentState {
    public void run();
    public void attack();
    public void takeCover();
    public void heal();
}
