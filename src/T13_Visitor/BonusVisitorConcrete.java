/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13_Visitor;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AK
 */
public class BonusVisitorConcrete implements BonusVisitor {

    @Override
    public void visit(Hero hero) {
        hero.xpPoints++;
        try {
            if (hero.state.getClass().equals(Class.forName("T13_Visitor.SecondState"))) {
                hero.xpPoints += 3;
            } else if (hero.state.getClass().equals(Class.forName("T13_Visitor.ThirdState"))) {
                hero.xpPoints += 5;
            }
        } catch (ClassNotFoundException ex) {
            System.out.println("Class not found");
        }
    }

}
