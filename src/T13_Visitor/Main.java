/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13_Visitor;

import T7_State.*;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) {
        Hero hero = new Hero();
        BonusVisitor visitor = new BonusVisitorConcrete();

        System.out.println("Olet aloittanut matkasi sankarina");
        System.out.println("");
        System.out.println("Taistellaan!");
        fight(hero);
        hero.accept(visitor);
        System.out.println("Sinulla on nyt " + hero.xpPoints + " kehityspistettä");

        hero.changeState(SecondState.getInstance());
        System.out.println("");
        System.out.println("Kehityit! Taistellaan lisää");
        fight(hero);
        hero.accept(visitor);
        System.out.println("Sinulla on nyt " + hero.xpPoints + " kehityspistettä");

        hero.changeState(ThirdState.getInstance());
        System.out.println("");
        System.out.println("Kehityit taas! Taistellaan viimeinen taistelu");
        fight(hero);
        hero.accept(visitor);
        System.out.println("Sinulla on nyt " + hero.xpPoints + " kehityspistettä");
    }

    public static void fight(Hero hero) {
        hero.attack();
        hero.takeCover();
        hero.heal();
        hero.run();
    }

}
