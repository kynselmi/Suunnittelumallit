/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T13_Visitor;

import T7_State.*;

/**
 *
 * @author AK
 */
public class FirstState implements DevelopmentState {

    private FirstState() {
    }

    private final static FirstState INSTANCE = new FirstState();

    public static FirstState getInstance() {
        return INSTANCE;
    }

    @Override
    public void run() {
        System.out.println("Run like a first timer");
    }

    @Override
    public void attack() {
        System.out.println("Attack a little");
    }

    @Override
    public void takeCover() {
        System.out.println("Trying to hide - Closing eyes");
    }

    @Override
    public void heal() {
        System.out.println("Heal up - Drinking water");
    }

}
