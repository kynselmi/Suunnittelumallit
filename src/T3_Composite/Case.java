/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T3_Composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aleksi
 */
public class Case implements Component {

    final List<Component> components = new ArrayList<>();

    @Override
    public int getPrice() {
        int sum = 0;
        for (Component c : components) {
            sum += c.getPrice();
        }
        return sum;
    }

    @Override
    public void addComponent(Component c) {
        components.add(c);
    }

}
