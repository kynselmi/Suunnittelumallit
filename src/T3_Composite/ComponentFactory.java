/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T3_Composite;

import java.util.List;

/**
 *
 * @author Aleksi
 */
public class ComponentFactory implements AbstractComponentFactory {

    @Override
    public Component createProcessor() {
        return new Processor();
    }

    @Override
    public Component createMemory() {
        return new Memory();
    }

    @Override
    public Component createMotherboard(List<Component> listOfComponents) {
        Motherboard mb = new Motherboard();
        for (Component c : listOfComponents) {
            mb.addComponent(c);
        }
        return mb;
    }

    @Override
    public Component createFan() {
        return new Fan();
    }

    @Override
    public Component createCase(List<Component> listOfComponents) {
        Case caseForComputer = new Case();
        for (Component c : listOfComponents) {
            caseForComputer.addComponent(c);
        }
        return caseForComputer;
    }

}
