/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T3_Composite;

import java.util.List;

/**
 *
 * @author Aleksi
 */
public interface AbstractComponentFactory {
    
    public Component createProcessor();
    public Component createMemory();
    public Component createMotherboard(List<Component> listOfComponents);
    public Component createFan();
    public Component createCase(List<Component> listOfComponents);
    
    
}
