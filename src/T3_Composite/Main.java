/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T3_Composite;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aleksi
 */
public class Main {
    
    public static void main(String[] args) {
        AbstractComponentFactory acf = new ComponentFactory();
        List<Component> motherBoardComponents = new ArrayList<>();
        
        System.out.println("Hankitaan tietokone: ");
        motherBoardComponents.add(acf.createProcessor());
        motherBoardComponents.add(acf.createMemory());
        motherBoardComponents.add(acf.createMemory());
        motherBoardComponents.add(acf.createFan());
        
        Motherboard mb = (Motherboard)acf.createMotherboard(motherBoardComponents);
        
        List<Component> caseComponents = new ArrayList<>();
        caseComponents.add(acf.createFan());
        caseComponents.add(acf.createFan());
        caseComponents.add(acf.createFan());
        caseComponents.add(mb);
        
        Case computer = (Case)acf.createCase(caseComponents);
        
        System.out.println("Hintaa tuli "+computer.getPrice()+"e");
    }
    
}
