/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T3_Composite;

/**
 *
 * @author Aleksi
 */
public interface Component {
    
    public int getPrice();
    public void addComponent(Component c);
    
}
