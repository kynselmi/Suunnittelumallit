/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T21_Facade;

/**
 *
 * @author AK
 */
public class FinishedGoodsStore implements Store {

    public Goods getGoods() {
        FinishedGoods finishedGoods = new FinishedGoods();
        return finishedGoods;
    }
}
