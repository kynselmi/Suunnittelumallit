/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T21_Facade;

/**
 *
 * @author AK
 */
public interface Store {
    
    public Goods getGoods();
    
}
