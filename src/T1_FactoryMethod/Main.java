package T1_FactoryMethod;

public class Main {

    public static void main(String[] args) {
        AterioivaOtus opettaja = new Opettaja();
        AterioivaOtus lapsi = new Lapsi();
        AterioivaOtus oppilas = new Oppilas();
        opettaja.aterioi();
        lapsi.aterioi();
        oppilas.aterioi();
    }
}
