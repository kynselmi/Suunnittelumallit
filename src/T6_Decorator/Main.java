package T6_Decorator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Tiedostokasittelija tk = new TiedostoKasittelijaDeco(new TiedostoKasittelijaToteutus());
        BufferedReader br = new BufferedReader(new FileReader(new File("decorator.txt")));

        System.out.println("Kirjoitetaan sana \"decorator\" tiedostoon");
        tk.kirjoita("decorator");
        String tiedostonSisalto = br.readLine();
        System.out.println("Kirjoitimme: " + tiedostonSisalto);
        System.out.println("Luemme decoratorin avulla niin saadaan jotain selvää: " + tk.lue());
        System.out.println("");
        
        //Ilman kryptausta
        tk = new TiedostoKasittelijaToteutus();
        System.out.println("Kirjoitetaan sana \"decorator\" tiedostoon");
        tk.kirjoita("decorator");
        br = new BufferedReader(new FileReader(new File("decorator.txt")));
        tiedostonSisalto = br.readLine();
        System.out.println("Kirjoitimme: " + tiedostonSisalto);
        System.out.println("Luemme Tiedostokasittelijan metodilla lue: " + tk.lue());

    }

}
