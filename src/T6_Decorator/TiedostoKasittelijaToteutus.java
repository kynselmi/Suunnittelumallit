/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6_Decorator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aleksi
 */
public class TiedostoKasittelijaToteutus implements Tiedostokasittelija {
    
    private BufferedReader lukija;
    private FileWriter kirjoittaja;
    private File tiedosto;
    
    public TiedostoKasittelijaToteutus() throws IOException {
        this.tiedosto = new File("decorator.txt");
        this.kirjoittaja = new FileWriter(tiedosto);
        this.lukija = new BufferedReader(new FileReader(tiedosto));
    }

    @Override
    public void kirjoita(String teksti) {
        try {
            this.kirjoittaja.write(teksti);
            this.kirjoittaja.close();
        } catch (IOException ex) {
            System.out.println("Tiedostoa ei löydy (kirjoitus)");
        }
    }

    @Override
    public String lue() {
        String luettu = "";
        try {
            luettu = this.lukija.readLine();
            lukija.close();
        } catch (IOException ex) {
            System.out.println("Tiedostoa ei löydy (lue)");
        }        
        return luettu;
        
    }
    
}
