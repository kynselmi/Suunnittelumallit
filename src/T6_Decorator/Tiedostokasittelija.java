/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6_Decorator;

/**
 *
 * @author Aleksi
 */
public interface Tiedostokasittelija {
    
    public void kirjoita(String teksti);
    public String lue();
    
}
