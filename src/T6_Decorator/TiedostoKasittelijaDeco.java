/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T6_Decorator;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Aleksi
 */
public class TiedostoKasittelijaDeco implements Tiedostokasittelija {

    private Tiedostokasittelija tk;

    public TiedostoKasittelijaDeco(Tiedostokasittelija tk) {
        this.tk = tk;
    }

    @Override
    public void kirjoita(String teksti) {
        tk.kirjoita(cryptaa(teksti));
    }

    @Override
    public String lue() {
        return cryptaa(tk.lue());

    }

    private String cryptaa(String teksti) {
        String cryptattu = "";
        for (int i = teksti.length() - 1; i >= 0; i--) {
            cryptattu += teksti.charAt(i);
        }
        return cryptattu;
    }

}
