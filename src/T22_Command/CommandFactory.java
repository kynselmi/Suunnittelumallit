/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T22_Command;

import java.util.HashMap;
import java.util.stream.Collectors;

public final class CommandFactory {

    private final HashMap<String, Command> commands;

    private CommandFactory() {
        commands = new HashMap<>();
    }

    public void addCommand(String name, Command command) {
        commands.put(name, command);
    }

    public void executeCommand(String name) {
        if (commands.containsKey(name)) {
            commands.get(name).apply();
        }
    }

    public void listCommands() {
        System.out.println("Käytettävät komennot: " + commands.keySet().stream().collect(Collectors.joining(", ")));
    }

    /* Factory pattern */
    public static CommandFactory init() {
        CommandFactory cf = new CommandFactory();

        
        cf.addCommand("ylos", () -> System.out.println("Valkokangas nostettu ylös"));
        cf.addCommand("alas", () -> System.out.println("Valkokangas laskettu alas"));

        return cf;
    }
}
