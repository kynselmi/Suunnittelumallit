/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T22_Command;

/**
 *
 * @author AK
 */
public class Client {
    public static void main(String[] args) {
        CommandFactory cf = CommandFactory.init();

        cf.listCommands();
        System.out.println("Tehdään komento 'alas'");
        cf.executeCommand("alas");
        
        System.out.println("Tehdään komento 'ylös'");
        cf.executeCommand("ylos");
    }
}
