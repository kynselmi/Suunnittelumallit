/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T12_Proxy;

/**
 *
 * @author Aleksi
 */
public interface Image {
    public void displayImage();
    public String showData();
}
