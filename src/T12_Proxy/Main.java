/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T12_Proxy;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Aleksi
 */
public class Main {

    public static void main(String[] args) {
        
        List<ProxyImage> lista = new ArrayList<>();
        
        for (int i = 0; i<5; i++) {
            lista.add(new ProxyImage("kuva"+(i+1)));
        }
        
        for (ProxyImage kuva : lista) {
            System.out.println("Tarkastellaan kuvan tietoja, nimi: "+kuva.showData()+"\n");
        }
        
        for (ProxyImage kuva : lista) {
            kuva.displayImage();
        }
    }
}
