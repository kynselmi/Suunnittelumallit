/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T12_Proxy;

/**
 *
 * @author Aleksi
 */
public class RealImage implements Image {

    private String filename = null;

    /**
     * Constructor
     *
     * @param filename
     */
    public RealImage(final String filename) {
        this.filename = filename;
        loadImageFromDisk();
    }

    /**
     * Loads the image from the disk
     */
    private void loadImageFromDisk() {
        System.out.println("Ladataan   " + filename);
    }

    /**
     * Displays the image
     */
    @Override
    public void displayImage() {
        System.out.println("Näytetään " + filename);
    }

    @Override
    public String showData() {
        return filename;
    }

}
