/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T5_Singleton;

import java.util.ArrayList;

/**
 *
 * @author AK
 */
public class Pizza {
    
    private ArrayList<String> toppings;
    
    public Pizza() {
        toppings = new ArrayList<>();
        toppings.add("Tomato Sauce");
        toppings.add("Cheese");
    }
    
    public void addMoreToppings() {
        System.out.println("Let's add toppings");
        if (toppings.size() < 3) {
            toppings.add("Tuna fish");
            System.out.println("Some tuna");
        } else if (toppings.size() < 4) {
            toppings.add("Jalapeno");
            System.out.println("Some hot pepper");
        } else {
            System.out.println("No more toppings, you are on a diet");
        }
    }
    
    @Override
    public String toString() {
        return "Your pizza: \n"+toppings;
    }
}
