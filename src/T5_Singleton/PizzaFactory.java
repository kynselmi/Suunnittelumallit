/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T5_Singleton;

/**
 *
 * @author AK
 */
public enum PizzaFactory {

    INSTANCE;

    private PizzaFactory() {
        System.out.println("Ready to make pizza");
    }

    ;
    
    public static PizzaFactory getInstance() {

        return INSTANCE;
    }

    public Pizza createPizza() {
        System.out.println("Let's make some pizza!");
        return new Pizza();
    }

}
