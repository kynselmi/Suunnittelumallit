/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T5_Singleton;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) throws ClassNotFoundException {
        System.out.println("Ei vielä loadattu");
        
        Class.forName("T5_Singleton.PizzaFactory");
        System.out.println("PizzaFactor-luokka ladattu");
        
        PizzaFactory pf = PizzaFactory.getInstance();

        Pizza pizza1 = pf.createPizza();
        pizza1.addMoreToppings();

        Pizza pizza2 = pf.createPizza();
        pizza2.addMoreToppings();
        pizza2.addMoreToppings();

        Pizza pizza3 = pf.createPizza();
        pizza3.addMoreToppings();
        pizza3.addMoreToppings();
        pizza3.addMoreToppings();
        
        System.out.println(pizza1);
        System.out.println(pizza2);
        System.out.println(pizza3);
    }

}
