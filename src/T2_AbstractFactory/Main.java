/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory;

import T2_AbstractFactory.Boss.BossFactory;
import T2_AbstractFactory.Adidas.AdidasFactory;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) {

        FactoryIF adidasF = new AdidasFactory();
        FactoryIF bossF = new BossFactory();

        Jasper jasperi = new Jasper(adidasF);
        jasperi.pue();
        
        System.out.println("");
        
        jasperi.setFactory(bossF);
        jasperi.pue();

    }

}
