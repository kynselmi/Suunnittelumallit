/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.Adidas;

import T2_AbstractFactory.Farkut;

/**
 *
 * @author AK
 */
public class AdidasFarkut implements Farkut {
    
    @Override
    public String toString() {
        return "Adidas-farkut";
    }
    
    @Override
    public void pue() {
        System.out.println("Housuina: "+this);
    }
}
