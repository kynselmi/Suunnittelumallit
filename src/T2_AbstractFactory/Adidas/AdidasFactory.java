/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.Adidas;

import T2_AbstractFactory.FactoryIF;
import T2_AbstractFactory.Farkut;
import T2_AbstractFactory.Kengat;
import T2_AbstractFactory.Lippis;
import T2_AbstractFactory.Paita;

/**
 *
 * @author AK
 */
public class AdidasFactory implements FactoryIF {

    @Override
    public Lippis luoLippis() {
        return new AdidasLippis();
    }

    @Override
    public Paita luoPaita() {
        return new AdidasPaita();
    }

    @Override
    public Farkut luoFarkut() {
        return new AdidasFarkut();
    }

    @Override
    public Kengat luoKengat() {
        return new AdidasKengat();
    }
    
}
