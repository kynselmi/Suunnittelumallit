/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.Adidas;

import T2_AbstractFactory.Kengat;

/**
 *
 * @author AK
 */
public class AdidasKengat implements Kengat {

    @Override
    public String toString() {
        return "Adidas-kengät";
    }

    @Override
    public void pue() {
        System.out.println("Kenkinä: " + this);
    }
}
