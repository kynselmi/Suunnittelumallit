/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.Adidas;

import T2_AbstractFactory.Lippis;

/**
 *
 * @author AK
 */
public class AdidasLippis implements Lippis {

    @Override
    public String toString() {
        return "Adidas-lippis";
    }

    @Override
    public void pue() {
        System.out.println("Päässä: " + this);
    }
}
