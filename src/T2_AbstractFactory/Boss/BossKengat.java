/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.Boss;

import T2_AbstractFactory.Kengat;

/**
 *
 * @author AK
 */
public class BossKengat implements Kengat {

    @Override
    public String toString() {
        return "Boss-kengät";
    }

    @Override
    public void pue() {
        System.out.println("Kenkinä: " + this);
    }
}
