/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.Boss;

import T2_AbstractFactory.Lippis;

/**
 *
 * @author AK
 */
public class BossLippis implements Lippis {

    @Override
    public String toString() {
        return "Boss-lippis";
    }

    @Override
    public void pue() {
        System.out.println("Päässä: " + this);
    }
}
