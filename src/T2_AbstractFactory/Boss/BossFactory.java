/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.Boss;

import T2_AbstractFactory.FactoryIF;
import T2_AbstractFactory.Farkut;
import T2_AbstractFactory.Kengat;
import T2_AbstractFactory.Lippis;
import T2_AbstractFactory.Paita;

/**
 *
 * @author AK
 */
public class BossFactory implements FactoryIF {

    @Override
    public Lippis luoLippis() {
        return new BossLippis();
    }

    @Override
    public Paita luoPaita() {
        return new BossPaita();
    }

    @Override
    public Farkut luoFarkut() {
        return new BossFarkut();
    }

    @Override
    public Kengat luoKengat() {
        return new BossKengat();
    }
    
}
