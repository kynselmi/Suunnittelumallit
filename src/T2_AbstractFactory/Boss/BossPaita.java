/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.Boss;

import T2_AbstractFactory.Paita;

/**
 *
 * @author AK
 */
public class BossPaita implements Paita {

    @Override
    public String toString() {
        return "Boss-paita";
    }

    @Override
    public void pue() {
        System.out.println("Paitana: " + this);
    }

}
