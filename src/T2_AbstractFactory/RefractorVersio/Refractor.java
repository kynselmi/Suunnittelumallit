/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory.RefractorVersio;

import T2_AbstractFactory.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AK
 */
public class Refractor {

    public static void main(String[] args) {
        Jasper jasper = new Jasper(fetchFactory());
        jasper.pue();

        System.out.println("\nVaihdetaan property-tiedoston sisältö\n");

        FileWriter fw = null;
        try {
            fw = new FileWriter(new File("factory.properties"));
            fw.write("factory = T2_AbstractFactory.Boss.BossFactory");
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(Refractor.class.getName()).log(Level.SEVERE, null, ex);
        }

        jasper.setFactory(fetchFactory());
        jasper.pue();
        
        try {
            fw = new FileWriter(new File("factory.properties"));
            fw.write("factory = T2_AbstractFactory.Adidas.AdidasFactory");
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(Refractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public static FactoryIF fetchFactory() {
        Class c = null;
        FactoryIF factory = null;

        Properties properties = new Properties();

        try {
            properties.load(new FileInputStream("factory.properties"));
            c = Class.forName(properties.getProperty("factory"));
            factory = (FactoryIF) c.newInstance();
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Refractor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return factory;
    }

}
