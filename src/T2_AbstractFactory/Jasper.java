/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T2_AbstractFactory;

/**
 *
 * @author AK
 */
public class Jasper {
    
    FactoryIF factory;
    
    public Jasper(FactoryIF factory) {
        this.factory = factory;
    }
    
    public void pue() {
        factory.luoLippis().pue();
        factory.luoPaita().pue();
        factory.luoFarkut().pue();
        factory.luoKengat().pue();
        
    }

    public void setFactory(FactoryIF factory) {
        this.factory = factory;
    }
    
    
}
