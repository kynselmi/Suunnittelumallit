/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T8_TemplateMethod;

import java.util.Random;

/**
 *
 * @author AK
 */
public class Kimble extends Game {

    private Player[] players;
    private String[] colors;
    private Random random;
    private Player winner;

    @Override
    void initializeGame() {
        this.colors = new String[]{"Red", "Blue", "Yellow", "Green"};
        players = new Player[playersCount];
        createPlayers();
        random = new Random();
    }

    @Override
    void makePlay(int player) {
        Player playerInTurn = players[player];
        System.out.println("Moved player " + playerInTurn.color + " " + (random.nextInt(5)+1) + " steps forward");
        System.out.println("In goal: "+playerInTurn.inGoal);
        if (random.nextInt(5) == 1) {
            playerInTurn.addOneToGoal();
        }
        
    }

    @Override
    boolean endOfGame() {
        for (int i = 0; i<playersCount; i++) {
            Player player = players[i];
            if (player.inGoal == 4) {
                winner = player;
                return true;
            }
        }
        return false;
    }

    @Override
    void printWinner() {
        System.out.println("Winner is: " + winner.color);
    }

    private void createPlayers() {
        for (int i = 0; i < playersCount; i++) {
            players[i] = new Player(colors[i]);
        }
    }

}
