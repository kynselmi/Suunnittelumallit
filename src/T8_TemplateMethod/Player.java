/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T8_TemplateMethod;

/**
 *
 * @author AK
 */
public class Player {
    
    public String color;
    protected int inBase;
    protected int inGoal;
    
    public Player(String color) {
        this.color = color;
        inBase = 4;
        inGoal = 0;
    }
    
    protected void addOneToGoal() {
        if (inGoal < 4) {
            inGoal++;
        }
    }
    
}
