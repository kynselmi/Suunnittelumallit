/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T8_TemplateMethod;

/**
 *
 * @author AK
 */
public class Main {
    public static void main(String[] args) {
        Game kimble = new Kimble();
        kimble.playersCount = 3;
        System.out.println("Let's play Kimble with three players");
        kimble.initializeGame();
        int n = 0;
        while(true) {
            kimble.makePlay(n%3);
            n++;
            if (kimble.endOfGame()) {
                break;
            }
        }
        kimble.printWinner();
    }
}
