/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T11_Memento;

/**
 *
 * @author AK
 */
public class Memento {
    
    private final int number;
    
    public Memento(int number) {
        this.number = number;
    }
    
    protected boolean vertaaArvausta(int arvaus) {
        return arvaus == number;
    }
    
}
