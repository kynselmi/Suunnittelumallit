/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T11_Memento;

import T11_MementoAsiakas.Asiakas;
import java.util.Random;

/**
 *
 * @author AK
 */
public class Randomizer {
    
    private Random random;
    
    public Randomizer() {
        random = new Random();
    }
    
    public void liityPeliin(Asiakas asiakas) {
        asiakas.setMemento(new Memento(random.nextInt(10)+1));
    }
    
    public boolean käsitteleArvaus(int arvaus, Memento memento) {
        return memento.vertaaArvausta(arvaus);
    }
    
}
