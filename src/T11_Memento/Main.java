/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T11_Memento;

import T11_MementoAsiakas.Asiakas;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) {

        Randomizer rd = new Randomizer();

        Asiakas a1 = new Asiakas(rd, "Pentti");
        Asiakas a2 = new Asiakas(rd, "Mauno");
        Asiakas a3 = new Asiakas(rd, "Kalevi");
        
        a1.start();
        a2.start();
        a3.start();

    }
}
