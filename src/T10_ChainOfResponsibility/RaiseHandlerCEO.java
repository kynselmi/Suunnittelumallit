/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10_ChainOfResponsibility;

/**
 *
 * @author AK
 */
public class RaiseHandlerCEO implements RaiseHandler{

    @Override
    public double handleRaise(double salary, double amountOfRaise) {
        System.out.println("Korotuksen hoiti: CEO");
        return salary*(1+amountOfRaise);
    }
    
}
