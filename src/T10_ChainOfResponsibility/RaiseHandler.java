/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10_ChainOfResponsibility;

/**
 *
 * @author AK
 */
public interface RaiseHandler {
    
    public double handleRaise(double salary, double amountOfRaise);
}
