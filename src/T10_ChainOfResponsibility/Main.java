/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10_ChainOfResponsibility;

/**
 *
 * @author AK
 */
public class Main {
    public static void main(String[] args) {
        int salary = 2000;
        System.out.println("Palkka on nyt: "+salary);
        System.out.println("Saat palkankorotuksen 1%");
        
        RaiseHandler rh = new RaiseHandlerSuperior();
        double newSalary = rh.handleRaise(salary, 0.01);
        System.out.println("Uusi palkka: "+newSalary);
        System.out.println("");
        
        System.out.println("Saat uuden korotuksen, tällä kertää 3.5%");
        newSalary = rh.handleRaise(newSalary, 0.035);
        System.out.println("Uusi palkka: "+newSalary);
        System.out.println("");
        
        System.out.println("Saat vielä lisää, nyt 10%");
        newSalary = rh.handleRaise(newSalary, 0.1);
        System.out.println("Uusi palkka: "+newSalary);
    }
    
}
