/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T10_ChainOfResponsibility;

/**
 *
 * @author AK
 */
public class RaiseHandlerSuperior implements RaiseHandler {

    private RaiseHandler nextHandler;
    
    public RaiseHandlerSuperior() {
        nextHandler = new RaiseHandlerManager();
    }

    @Override
    public double handleRaise(double salary, double amountOfRaise) {
        if (amountOfRaise <= 0.02) {
            System.out.println("Korotuksen hoiti: Superior");
            return salary*(1+amountOfRaise);
        } else {
            return nextHandler.handleRaise(salary, amountOfRaise);
        }
    }



}
