/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T4_Observer;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author AK
 */
public class DigitalClock implements Observer {

    @Override
    public void update(Observable o, Object arg) {
        //Näkymä kutsuu observoitavan toStringiä suoraan
        //System.out.println("Kello on: "+o);
        
        ClockTimer ct = (ClockTimer)o;
        System.out.println("Kello on: "+ct.getHour()+":"+ct.getMinute()+":"+ct.getSecond());
    
        //Näytettävä kama annettu suoraan
        //System.out.println(arg);
}
    
    
    
}
