/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T4_Observer;

import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AK
 */
public class ClockTimer extends Observable implements Runnable {

    private int second = 0;
    private int minute = 0;
    private int hour = 0;

    public void tick() {
        second++;
        if (second > 59) {
            second = 0;
            minute++;
            if (minute > 59) {
                minute = 0;
                hour++;
            }
            if (hour > 23) {
                hour = 0;
            }
        }
    }

    @Override
    public void run() {
        while (true) {
            tick();
            setChanged();
            notifyObservers(this.toString());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClockTimer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public String toString() {
        String s = "";
        s += hour + ":";
        if (minute < 10) {
            s += 0;
        }
        s += minute + ":";
        if (second < 10) {
            s += 0;
        }
        s += second;
        return s;
    }

    public int getSecond() {
        return second;
    }

    public int getMinute() {
        return minute;
    }

    public int getHour() {
        return hour;
    }
    
    

}
