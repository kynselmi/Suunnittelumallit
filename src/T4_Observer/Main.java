/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T4_Observer;

import java.util.Scanner;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) {

        ClockTimer clocktimer = new ClockTimer();
        DigitalClock digitalclock = new DigitalClock();

        clocktimer.addObserver(digitalclock);

        new Thread(clocktimer).start();



    }
}
