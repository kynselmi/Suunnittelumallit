/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14_Builder;

/**
 *
 * @author AK
 */
public class HesburgerBuilder implements HamburgerBuilder {

    private final StringBuilder sBuilder = new StringBuilder();

    @Override
    public void addBread() {
        sBuilder.append("Bread\n");
    }

    @Override
    public void addLettuce() {
        sBuilder.append("Lettuce\n");
    }

    @Override
    public void addTomato() {
        sBuilder.append("Tomato\n");
    }

    @Override
    public void addMeat() {
        sBuilder.append("Meat\n");
    }

    @Override
    public void addMajonese() {
        sBuilder.append("Majonese\n");
    }

    @Override
    public void addKetchup() {
    }

    @Override
    public void addPickels() {
        sBuilder.append("Pickels\n");
    }

    @Override
    public void addCheese() {
        sBuilder.append("Cheese\n");
    }

    @Override
    public Object getBurger() {
        return sBuilder;
    }

}
