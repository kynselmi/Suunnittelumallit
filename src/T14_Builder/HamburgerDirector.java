/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14_Builder;

/**
 *
 * @author AK
 */
public class HamburgerDirector {
    
    private HamburgerBuilder builder;
    
    public HamburgerDirector(HamburgerBuilder builder) {
        this.builder = builder;
    }
    
    public void constructBurger() {
        builder.addBread();
        builder.addLettuce();
        builder.addCheese();
        builder.addKetchup();
        builder.addMeat();
        builder.addPickels();
        builder.addTomato();
        builder.addBread();
    }
}
