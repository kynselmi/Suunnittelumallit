/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14_Builder;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author AK
 */
public class McDonaldsBuilder implements HamburgerBuilder {

    private final List<String> parts = new ArrayList<>();

    @Override
    public void addBread() {
        parts.add("Bread");
    }

    @Override
    public void addLettuce() {
        parts.add("Lettuce");
    }

    @Override
    public void addTomato() {
        parts.add("Tomato");
    }

    @Override
    public void addMeat() {
        parts.add("Meat");
    }

    @Override
    public void addMajonese() {
    }

    @Override
    public void addKetchup() {
        parts.add("Ketchup");
    }

    @Override
    public void addPickels() {
        parts.add("Pickels");
    }
    
    @Override
    public void addCheese() {
        parts.add("Cheese");
    }

    @Override
    public Object getBurger() {
        return parts;
    }

}
