/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T14_Builder;

import java.util.List;

/**
 *
 * @author AK
 */
public class Main {

    public static void main(String[] args) {
        HamburgerBuilder mBuilder = new McDonaldsBuilder();
        HamburgerBuilder hBuilder = new HesburgerBuilder();
        HamburgerDirector hDirector = new HamburgerDirector(mBuilder);
        System.out.println("Creating McDonalds burger");
        hDirector.constructBurger();
        System.out.println("McDonalds burger includes : "+mBuilder.getBurger());
        System.out.println("");

        System.out.println("Creating Hesburger burger");
        hDirector = new HamburgerDirector(hBuilder);
        hDirector.constructBurger();
        System.out.println("Hesburger burger includes: \n"+hBuilder.getBurger());
    }

}
