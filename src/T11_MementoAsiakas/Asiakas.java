/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package T11_MementoAsiakas;

import T11_Memento.Memento;
import T11_Memento.Randomizer;

/**
 *
 * @author AK
 */
public class Asiakas extends Thread {

    private Memento memento;
    private final Randomizer randomizer;
    private String name;
    
    public Asiakas(Randomizer randomizer, String name) {
        this.randomizer = randomizer;
        this.name = name;
    }

    @Override
    public void run() {
        randomizer.liityPeliin(this);
        int arvaus = 1;
        while (true) {            
            if (randomizer.käsitteleArvaus(arvaus, memento)) {
                System.out.println(name+" arvasi oikein");
                break;
            }
            arvaus++;
        }
    }

    public void setMemento(Memento memento) {
        this.memento = memento;
    }
    
    

}
